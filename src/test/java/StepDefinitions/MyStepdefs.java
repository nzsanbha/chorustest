/*
 * Classname : MyStepdefs.java
 *
 * Version information : 1.0
 *
 * Date : 30/01/2020
 */
package StepDefinitions;

import CommonFunctions.SelFunctions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MyStepdefs {
    private final SelFunctions sf = new SelFunctions();
    @Given("^An Internet user access Chorus broadband map page$")
    public void anInternetUserAccessChorusBroadbandMapPage() {
        System.out.println("Going to access headless browser.");
        sf.openBrowser();
        System.out.println("Going to access given url of broadband map.");
        sf.openWebpage();
    }

    @And("^enters his home address$")
    public void entersHisHomeAddress() {
        System.out.println("Going to enter user home address.");
        sf.enterAddressAndClick();

    }

    @When("^Internet user clicks the address suggested in dropdown$")
    public void internetUserClicksTheAddressSuggestedInDropdown() {
        //Empty Function. It can be enhanced to match the value provided in search input box.
    }

    @Then("^available services provided by Chorus at that location will be displayed$")
    public void availableServicesProvidedByChorusAtThatLocationWillBeDisplayed() {
        System.out.println("Going to verify services provided by Chorus at that location.");
        sf.availableBroadbandServices();
        System.out.println("Closing down the browser");
        //sf.teardown();
        System.out.println("Test successfully executed.");
    }
}
