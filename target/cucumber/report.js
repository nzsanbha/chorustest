$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("broadband.feature");
formatter.feature({
  "line": 1,
  "name": "In this feature several tests will be executed.",
  "description": "",
  "id": "in-this-feature-several-tests-will-be-executed.",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "As an Internet User, I want to know which Chorus services are available to me, So that I can decide on the service I want to order.",
  "description": "",
  "id": "in-this-feature-several-tests-will-be-executed.;as-an-internet-user,-i-want-to-know-which-chorus-services-are-available-to-me,-so-that-i-can-decide-on-the-service-i-want-to-order.",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 2,
      "name": "@runtest"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "An Internet user access Chorus broadband map page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "enters his home address",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "Internet user clicks the address suggested in dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "available services provided by Chorus at that location will be displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.anInternetUserAccessChorusBroadbandMapPage()"
});
formatter.result({
  "duration": 14424833300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.entersHisHomeAddress()"
});
formatter.result({
  "duration": 2111714300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.internetUserClicksTheAddressSuggestedInDropdown()"
});
formatter.result({
  "duration": 20900,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.availableServicesProvidedByChorusAtThatLocationWillBeDisplayed()"
});
formatter.result({
  "duration": 429558500,
  "status": "passed"
});
});