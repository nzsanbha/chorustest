/*

 * Classname : SelFunctions.java
 *
 * Version information : 1.0
 *
 * Date : 30/01/2020

 */
package CommonFunctions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SelFunctions {

    public static WebDriver driver = null;
    private static WebDriverWait waitVar = null;
    private static final java.lang.String webPageURL = "https://chorus.co.nz/broadband-map";

    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--window-size=1920,1080","--ignore-certificate-errors"); //"--headless", "--disable-gpu",
        driver = new ChromeDriver(options);
        System.out.println("Headless browser accessed.");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        waitVar = new WebDriverWait(driver, 15);
    }

    public void openWebpage() {
        driver.get(webPageURL);
    }

    public void enterAddressAndClick() {
        driver.findElement(By.xpath("//*[@id=\"wivolo-search-input\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"wivolo-search-input\"]")).sendKeys("116 Birkdale Road, Birkdale, North Shore");
        WebElement searchResult1 = driver.findElement(By.xpath("/html/body/ul/li[1]/div[@class='ui-menu-item-wrapper']"));
        searchResult1.click();
    }


    public void availableBroadbandServices() {

        List<WebElement> allElements = driver.findElements(By.xpath("//*[@id=\"location-details-now\"]/ul/li"));
        System.out.println("Following services are available at selected location.");
        for (WebElement element: allElements) {
            System.out.println(element.getText());
        }
    }

    public void teardown() {
        driver.quit();
    }

}
