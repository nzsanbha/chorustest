/*
 * Classname : RunTest.java
 *
 * Version information : 1.0
 *
 * Date : 30/01/2020
*/
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/featureFiles",
        glue = "StepDefinitions",
        plugin = {
                "pretty",
                "html:target/cucumber",
                "json:target/cucumber.json"
        }
)
public class RunTest {
}