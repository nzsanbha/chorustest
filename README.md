----------------------------------------------------------------------------------------------------------------------------------------------

## How to run this project ?

**Pre-requisite:** User should be using Windows based (fairly upgraded) machine with Maven and Java JDK installed on it.

Steps:  
1. Download above project by clicking three dots at top right corner (...) and select **Download Repository**.  
2. Project will get downloaded in a ZIP file. Unzip dowloaded project at local system folder.  
3. Now open command prompt and run < mvn test -Dcucumber.options="--tags @runtest" > without angular brackets.  
4. Finally after successful execution, available services at a specific address will be displayed at command prompt.  

**Note:** : No validations has been placed in code as its a generic framework and can be added later.

## Following scenario has been automated

Feature: In this featurefollowing test will be executed.
@runtest  
  Scenario: As an Internet User, I want to know which Chorus services are available to me, So that I can decide on the service I want to order.  
    Given An Internet user access Chorus broadband map page  
    And enters his home address  
    When Internet user clicks the address suggested in dropdown  
    Then available services provided by Chorus at that location will be displayed  
	
----------------------------------------------------------------------------------------------------------------------------------------------
