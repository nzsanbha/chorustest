Feature: In this feature several tests will be executed.
@runtest
  Scenario: As an Internet User, I want to know which Chorus services are available to me, So that I can decide on the service I want to order.
    Given An Internet user access Chorus broadband map page
    And enters his home address
    When Internet user clicks the address suggested in dropdown
    Then available services provided by Chorus at that location will be displayed


